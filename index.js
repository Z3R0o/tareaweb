'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Product = require('./modelos/product')
const app = express()

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

app.get('/hola',(req, res)=>{

    res.status(200).send({message:"Bienvenido"})

})

app.get('/api/product', (req, res)=>{

    res.status(200).send('Aqui devolveremos los productos')
})

app.get('/api/product/:productId', (req,res)=>{

    let ProductId = req.params.ProductId
    Product.findById(ProductId,(err,product)=>{

        if(err) return res.status(500).send({message:'error al realizar la peticion'})
        if(!product) return res.status(404).send({message:'Error el producto no existe'})

        res.status(200).send({product})
    })

    //res.status(200).send(`Aqui devolveremos el producto: ${req.params.productId}`)
})

app.post('/api/product', (req,res)=>{

    let product = new Product()
    product.name = req.body.name
    product.picture = req.body.picture
    product.price = req.body.price
    product.category = req.body.category
    product.description = req.body.description

    product.save((err,productStore)=>{

        if(err) res.status(500).send(`Error base de datos: ${err}`)

        res.status(200).send({product:productStore})
    })

    //res.status(200).send(`Producto:${req.body.name}`)

})

//funcion actualizar
app.put('/api/product/:productId', (req,res)=>{
  if(!req.body){
    return res.status(400).send({
        message: "El dato a Actualizar no puede estar vacio!"
    });
  }

  let ProductId = req.params.ProductId
  Product.findByIdAndUpdate(ProductId, req.body, {useFindModify: false})
    .then(data =>{
      if(!data){
        res.status(404).send({
          message: `No se pudo actualizar el producto con id ${ProductId}`
        })
      }else res.send({message: "El producto fue Actualizado"})
    })
    .catch(err =>{
      res.status(500).send({
        message: `Error al actualizar el producto con id ${ProdcutId}`
      })
    })

})


app.delete('/api/product/:productId', (req,res)=>{

  let ProductId = req.params.ProductId
  Product.findByIdAndRemove(ProductId)
  .then(data =>{
    if(!data){
      res.status(404).send({
        message: `No se pudo eliminar el producto con id ${ProductId}`
      })
    }else{
      res.send({
        message: "Producto eliminado exitosamente"
      })
    }
  })
  .catch(err =>{
    res.status(500).send({
      message: `Error al eliminar el producto con id ${ProductId}`
    })
  })

})

mongoose.connect('mongodb+srv://seba:sebish619@cluster0-aaj1n.gcp.mongodb.net/test?retryWrites=true&w=majority',(err,res)=>{

    if(err) throw err
    console.log('conexion establecida')

    app.listen(3000,()=>{

        console.log("Esta corriendo en el puerto 3000")
    })
})
